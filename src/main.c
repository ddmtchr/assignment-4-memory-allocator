#define _GNU_SOURCE

#include "mem.h"
#include "mem_internals.h"
#include "util.h"

#include <assert.h>
#include <sys/mman.h>

#define RUN_TEST(test_function) \
        printf("Running test: %s\n", #test_function); \
        test_function(); \
        printf("Test %s completed\n\n", #test_function); \

static void* map_pages(void const* addr, size_t length, int additional_flags) {
    return mmap( (void*) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags , -1, 0 );
}

static struct block_header* block_get_header(void* contents) {
    return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}

static void test_successful_allocation() {
    void* heap = heap_init(0);
    puts("Heap before allocation:");
    debug_heap(stdout, heap);

    void* mem = _malloc(0);
    puts("Heap after allocation:");
    debug_heap(stdout, heap);

    assert(mem != NULL);

    _free(mem);
    puts("Heap after freeing:");
    debug_heap(stdout, heap);
    heap_term();
}

static void test_free_single_block() {
    void* heap = heap_init(0);
    puts("Heap before allocation:");
    debug_heap(stdout, heap);

    void* mem1 = _malloc(64);
    void* mem2 = _malloc(128);
    puts("Heap after allocation:");
    debug_heap(stdout, heap);

    _free(mem1);
    puts("Heap after freeing one of blocks:");
    debug_heap(stdout, heap);

    assert(mem2 != NULL);
    _free(mem2);
    heap_term();
}

static void test_free_multiple_blocks() {
    void* heap = heap_init(0);
    puts("Heap before allocation:");
    debug_heap(stdout, heap);

    void* mem1 = _malloc(64);
    void* mem2 = _malloc(128);
    void* mem3 = _malloc(256);
    void* mem4 = _malloc(512);
    puts("Heap after allocation:");
    debug_heap(stdout, heap);

    _free(mem1);
    _free(mem3);

    assert(mem2 != NULL);
    assert(mem4 != NULL);
    puts("Heap after freeing some of blocks:");
    debug_heap(stdout, heap);

    _free(mem2);
    _free(mem4);
    heap_term();
}

static void test_same_region_extension() {
    struct region* heap = heap_init(0);
    puts("Heap before allocation:");
    debug_heap(stdout, heap);

    size_t mem1_size = 4096;
    void* mem1 = _malloc(mem1_size);

    void* mem2 = _malloc(REGION_MIN_SIZE * 2);
    assert(mem2 - mem1_size - offsetof(struct block_header, contents) == mem1);
    puts("Heap after allocation:");
    debug_heap(stdout, heap);

    _free(mem1);
    _free(mem2);
    heap_term();
}

void test_different_region_extension() {
    heap_init(0);
    void* mmap_addr = map_pages(HEAP_START, 10, MAP_FIXED);
    assert(mmap_addr);
    puts("Heap before allocation:");
    debug_heap(stdout, HEAP_START);

    void* mem1 = _malloc(REGION_MIN_SIZE * 2);
    puts("Heap after allocation:");
    debug_heap(stdout, HEAP_START);

    struct block_header* mem1_header = block_get_header(mem1);
    assert(mem1_header != mmap_addr);

    _free(mem1);
    heap_term();
}


int main() {
    RUN_TEST(test_successful_allocation)
    RUN_TEST(test_free_single_block)
    RUN_TEST(test_free_multiple_blocks)
    RUN_TEST(test_same_region_extension)
    RUN_TEST(test_different_region_extension)

    return 0;
}